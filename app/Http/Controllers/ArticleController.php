<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use App\Http\Requests;
use App\Articles;
use App\Http\Resources\Articles as ArticlesResource;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get article list 
        $articles = Articles::orderBy("created_at","desc")->paginate(5);

        // return article as a resource 
        return ArticlesResource::collection($articles);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // save an article 
        $article = $request->isMethod('put')?Articles::findOrFail($request->article_id):new Articles;

        $article->id = $request->input('article_id');
        $article->title = $request->input('title');
        $article->body = $request->input('body');

        if($article->save()){
            return new ArticlesResource($article);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) 
    { 
        // Get article
        $article = Articles::findOrFail($id);

        // return as a resource 
        return new ArticlesResource($article);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Get article
        $article = Articles::findOrFail($id);

        // return deleted resource 
        if($article->delete()){
            return new ArticlesResource($article);
        }
    }
}
